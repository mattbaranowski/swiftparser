//
//  ParserGeneratorGrammar.swift
//  Parser
//
//  Created by MattBaranowski on 10/7/14.
//  Copyright (c) 2014 MFB. All rights reserved.
//

import Foundation

infix operator =>> {}
infix operator ∣ {
associativity left
precedence 150
}
postfix operator ?? { }
postfix operator +  { }
postfix operator *%  { }
prefix operator % { }
prefix operator &%  { }
prefix operator !% { }


//MARK: Parser Grammar

class GrammarParserImpl : ParserImpl {
    var identifiers : [String] = []
    var literals : [String] = []
    var stack : [Node] = []
    var rules : Dictionary<String, AnyObject> = [:]
}

class Node { }
class Rule : Node { var name : String = ""; var variables : [Node] = []; var expression : Node?; }
class Expression : Node { }

class StringLiteralExpression : Node {
    var string : String = "";
    init(str: String) {
        string = str
    }
}

struct CharacterRange {
    var fromChar : Character;
    var toChar : Character;
}

class CharacterClassExpression : Node {
    var ranges : Array<CharacterRange> = Array<CharacterRange>();
}

class ExpressionList : Expression {
    var list : [Expression] = []
    func append( e : Expression ) { self.list.append(e) }
}

class SequenceNode : ExpressionList { }
class ChoiceNode  : ExpressionList  { }

func grammarAction( f : (GrammarParserImpl, String) -> Bool  ) -> (Parser, Range<String.Index>) -> Bool
{
    return { (p : Parser, range : Range<String.Index>) -> Bool in
        if let g = p as? GrammarParserImpl {
            return f(g, p.substr(range))
        }
        return false;
    }
}

func addNodeToStack( f : (String) -> Node? ) -> ((Parser, Range<String.Index>) -> Bool)
{
    return { (p : Parser, range : Range<String.Index>) -> Bool in
        if let g = p as? GrammarParserImpl {
            if let node = f(p.substr(range)) {
                g.stack.append( node )
            }
            return true
        }
        return false;
    }
}

func addToExpressionList(g : GrammarParserImpl, type : ExpressionList.Type) -> Bool
{
    if let e = g.stack.removeLast() as? Expression {
        if let seq = g.stack.last as? ChoiceNode {
            seq.list.append(e)
        } else {
            var seq = ChoiceNode()
            seq.list.append( e )
            g.stack.append( seq )
        }
        return true
    }
    
    return false
}


func locally(work: () -> ()) {
    work()
}




func UnitTestLiteral( p : ParserBlock) -> Bool {
    
    println("unit test listerals")
    assert( p( GrammarParserImpl(buffer: "no quotes") ) == false, "reject without parenthesis" );
    assert( p( GrammarParserImpl(buffer: "'quoted\"")) == false, "reject different end and start quotes" );
    assert( p( GrammarParserImpl(buffer: "\"quoted\'")) == false, "reject different end and start quotes" );
    
    assert( p( GrammarParserImpl(buffer: "'quoted'")) == true, "accept single quoted string" );
    
    locally {
        let g = GrammarParserImpl(buffer: "\"quoted\"")
        assert( p( g ) == true, "reject without parenthesis" )
        assert( g.literals.first! == "quoted", "captured the correct parameter" );
    }
    
    locally {
        let g = GrammarParserImpl(buffer: "\"quoted\\\"\"")
        assert( p( g ) == true, "accept double quotes with escaped quotes" );
        assert( g.literals.first! == "quoted\\\"", "captured the correct parameter" );
    }
    
    return true
}



func ParserGrammar() -> ParserBlock
{
    let SPACE = ParseOptionalSpaceOrSingleLineComment("#")
    func ps(c : Character) -> ParserBlock {
        return seq( ParseChar(c), SPACE );
    }
    
    let EQUAL =    ps("=")
    let BAR =      ps("|")
    let AND =      ps("&")
    let NOT =      ps("!")
    let QUESTION = ps("?")
    let STAR =     ps("*")
    let PLUS =     ps("+")
    let OPEN =     ps("(")
    let CLOSE =    ps(")")
    let COLON =    ps(":")
    let DOT =      ps(".")
    let BEGIN_CAPTURE = ps("<")
    let END_CAPTURE = ps(">")
    
    var expression = ParseReference()
    
    let identHead = ParseCharacterInRange("a", "z") | ParseCharacterInRange("A", "Z") | %"_"
    let identTail = identHead | ParseCharacterInRange("0", "9")
    let identifier = seq( identHead, identTail*% ) =>>
        {
            (p : Parser, range : Range<String.Index>) -> Bool in
            if let g = p as? GrammarParserImpl {
                g.identifiers.append( p.substr(range) )
            }
            return true
    }
    
    let notBraces = seq( !%"}", ParseAnyChar )
    let inBraces = seq( %"{", notBraces*%, %"}" ) | notBraces
    
    let char = %[ %"\\", ParseCharacterInSet("abefnrtv'\"[]\\") ] |
        %[ !%"\\", ParseAnyChar ]
    
    let literalAction =  { (p : Parser, range : Range<String.Index>) -> Bool in
        if let g = p as? GrammarParserImpl {
            g.literals.append( p.substr(range) )
        }
        return true
    }
    
    let literal =
     (%[%"'",
        (%[ !%"'", char])*% =>> literalAction,
        %"'" ]
        |
        %[%"\"",
            (%[ !%"\"", char])*% =>> literalAction,
            %"\"" ] ) =>> addNodeToStack { (str : String) -> Node in
                return StringLiteralExpression(str: str);
            }
    
    let appendRange = grammarAction({ (g : GrammarParserImpl, s : String) -> Bool in
        
        let fromTo = s.componentsSeparatedByString("-")
        
        let r: CharacterRange;
        if fromTo.count > 1 {
            r = CharacterRange(fromChar: Array(fromTo[0])[0], toChar: Array(fromTo[1])[0])
        } else {
            let char = Array(fromTo[0])[0]
            r = CharacterRange(fromChar: char, toChar: char)
        }
        
        if let c = g.stack.removeLast() as? CharacterClassExpression {
            c.ranges.append(r)
        } else {
            var classExpr = CharacterClassExpression()
            classExpr.ranges.append(r)
            g.stack.append(classExpr)
        }
        return true
    })
    
    let range = choice(
        seq( char, %"-", char ) =>> appendRange, char)
    
    let charClass =
    %[%"[",
        (%[ !%"]", range])*% =>> literalAction,
        %"]"]
    
    let action = seq( %"{", inBraces*%, %"}", SPACE )
    
    let primary = choice(
        seq( identifier, COLON, identifier, !%EQUAL )
        ,seq( identifier, !%EQUAL )
        ,seq( OPEN, &%expression, CLOSE )
        ,literal
        ,charClass
        ,DOT
        ,action
        ,BEGIN_CAPTURE
        ,END_CAPTURE )
    
    let suffix = seq( primary, choice(QUESTION, STAR, PLUS) )
    
    let prefix = choice(seq(AND, action), seq(AND, suffix), seq(NOT, suffix), suffix)
    
    let sequence = seq( prefix, (prefix =>> grammarAction { (g : GrammarParserImpl, s : String) in
        if let e = g.stack.removeLast() as? Expression {
            if let seq = g.stack.last as? SequenceNode {
                seq.list.append(e)
            } else {
                var seq = SequenceNode()
                seq.list.append( e )
                g.stack.append( seq )
            }
            return true
        }
        
        return false
        })*% )
    
    expression.block = seq( sequence, (!%[BAR,
        sequence =>> grammarAction { (g : GrammarParserImpl, s : String) in
            if let e = g.stack.removeLast() as? Expression {
                if let seq = g.stack.last as? ChoiceNode {
                    seq.list.append(e)
                } else {
                    var seq = ChoiceNode()
                    seq.list.append( e )
                    g.stack.append( seq )
                }
                return true
            }
            
            return false
        }
        ])*% )
    
    let definition = seq(
        identifier =>> grammarAction { (g : GrammarParserImpl, s : String) in
            if (g.rules[s] != nil) {
                println("duplicate definition for rule \(s)")
                return false
            }
            var rule = Rule()
            rule.name = s
            g.rules[s] = rule;
            g.stack.append( rule )
            return true
        },
        SPACE, EQUAL,
        expression.block =>> grammarAction { (g : GrammarParserImpl, s : String) in
            let n = g.stack.removeLast()
            if let rule = g.stack.removeLast() as? Rule {
                rule.expression = n
            }
            return true
        }
    )
    
    return definition
}


