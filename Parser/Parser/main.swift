//
//  main.swift
//  Parser
//
//  Created by OfficeVR on 9/14/14.
//  Copyright (c) 2014 MFB. All rights reserved.
//

import Foundation

protocol MathASTNode {
    func value() -> Int
}

protocol Parser {
    func nextChar();
    func isEOF() -> Bool;
    func current() -> Character;
    func pos() -> String.Index;
    func setPos(p : String.Index);
    func substr(r : Range<String.Index>) -> String;
}

class ParserImpl : Parser
{
    private var buffer : String
    private var _pos : String.Index;
    
    func pos() -> String.Index { return self._pos; }
    func setPos(p : String.Index) { self._pos = p; }

    func current() -> Character { return self.buffer[ self._pos ] }
 
    
    func nextChar() {
        self._pos = self._pos.successor()
    }
    
    func isEOF() -> Bool {
        return self._pos >= self.buffer.endIndex
    }
    
    init(buffer : String) {
        self.buffer = buffer
        self._pos = self.buffer.startIndex
    }
    
    func substr(r : Range<String.Index>) -> String {
        return self.buffer.substringWithRange(r)
    }
}

func createParserFromFile<T: ParserImpl>(filename : String) -> T? {
    if let buffer = String(contentsOfFile:filename, encoding: NSUTF8StringEncoding, error: nil)
    {
        return T(buffer: buffer)
    } else {
        return nil
    }
}

typealias ParserBlock = (Parser) -> Bool

let ParseAnyChar = { (p:Parser) -> Bool in
    if p.isEOF() {
        return false
    }
    p.nextChar()
    return true
}

func ParseChar(c : Character)(p : Parser) -> Bool {
    if p.isEOF() {
        return false
    }
    
    if p.current() == c {
        p.nextChar()
        return true
    }
    
    return false
}

func ParseAnyCharInString(str : String)(p : Parser) -> Bool {

        if p.isEOF() {
            return false
        }
        
        for strChar in str {
            if p.current() == strChar {
                p.nextChar()
                return true
            }
        }
        
        return false
}

func ParseCharacterInRange(start : Character, end : Character)(p : Parser) -> Bool
{
    if p.isEOF() {
        return false
    }
    
    var c = p.current()
    if c >= start && c <= end {
        p.nextChar()
        return true;
    }
    
    return false
}

func ParseCharacterInSet(chars : String)(p : Parser) -> Bool
{
    if p.isEOF() {
        return false
    }
    
    var c = p.current()
    for t in chars {
        if t == c {
            p.nextChar()
            return true;
        }
    }
    
    return false
}

func ParseString(str : String)(p: Parser) -> Bool {
    if p.isEOF() {
        return false
    }
    
    var strIdx = str.startIndex
    let savedIndex = p.pos()
    for strChar in str {
        if p.isEOF() || !(strChar == p.current()) {
            p.setPos(savedIndex)
            return false
        }
        p.nextChar()
    }
    
    return true
}

func ParseOptionalSpaceOrSingleLineComment(delimiter : Character)(p: Parser) -> Bool {
    while !p.isEOF() {
        let c = p.current()
        switch c {
        case " ", "\t", "\r", "\n":
            p.nextChar()
        case delimiter:
            p.nextChar()
            while !p.isEOF() && p.current() != "\n" || p.current() != "\r" {
                p.nextChar()
            }
        default:
            return true
        }
    }
    
    return true
}

func ParseZeroOrMore(block : ParserBlock)(p : Parser) -> Bool {
    while block(p) { }
    return true;
}

func ParseOneOrMore(block : ParserBlock)(p : Parser) -> Bool {
    if !block(p) {
        return false;
    }
    while block(p) { }
    return true
}

func ParseOptional(block : ParserBlock)(p : Parser) -> Bool {
    block(p)
    return true
}

func ParseNotPredicate(block : ParserBlock)(p : Parser) -> Bool {
    let savePos = p.pos()
    let result = block(p)
    p.setPos(savePos)
    return !result
}

func ParseAndPredicate(block : ParserBlock)(p : Parser) -> Bool {
    let savePos = p.pos()
    let result = block(p)
    p.setPos(savePos)
    return result
}

typealias ParseActionBlock = (Parser, Range<String.Index>) -> Bool

func ParseAction(block : ParserBlock, action : ParseActionBlock)(p : Parser) -> Bool {
    
    var range = Range<String.Index>(start: p.pos(), end: p.pos())
    let result = block(p)
    range.endIndex = p.pos()
    
    if result {
        return action(p, range)
    }
    return result
}

func ParseAction(block : ParserBlock, action : () -> ())(p : Parser) -> Bool {
    
    let result = block(p)
    if (result) {
        action()
    }
    return result
    
}

func ParseSequence(blocks : [ParserBlock])(p : Parser) -> Bool {
    let savePos = p.pos()
    for block in blocks {
        if !block(p) {
            p.setPos(savePos)
            return false
        }
    }
    return true
}

func ParseChoice(blocks : [ParserBlock])(p : Parser) -> Bool {
    var savePos = p.pos()
    for block in blocks {
        if block(p) {
            return true
        }
        
        p.setPos(savePos)
    }
    return false
}


let ParseAssert = { (p:Parser) -> Bool in
    assertionFailure("should never reach this block")
    return false
}

class ParseReference {
    var block : ParserBlock = ParseAssert
}

func ParseWithReference( ref : ParseReference)(p : Parser) -> Bool {
    return ref.block(p)
}


func ParseDebug(block : ParserBlock, name : String)(p : Parser) -> Bool {
    println("start \(name)")
    
    let result = block(p);
    
    println("end \(name) - \(result)")
    return result;
}

infix operator =>> {}
infix operator ∣ {
associativity left
precedence 150
}
postfix operator ?? { }
postfix operator +  { }
prefix operator  %   { }
prefix operator  &%  { }
prefix operator  !% { }
postfix operator *% { }

func =>> (block : ParserBlock, debugName : String) -> ParserBlock {
    return ParseDebug(block, debugName)
}


// all the crazy operators that swift supports
// U+00A1–U+00A7: ¡¢£¤¥¦
// U+00A9 or U+00AB © «
// U+00AC or U+00AE ¬ ®
// U+00B0–U+00B1, ° ± U+00B6 ¶ U+00BB » U+00BF ¿ U+00D7 × or U+00F7 ÷
// U+2016–U+2017 ‖ ‗ or U+2020–U+2027 †‡•‣․‥…‧
// U+2030–U+203E ‰‱′″‴‵‶‷‸‹›※‼‽‾
// U+2041–U+2053 ⁁⁂⁃⁄⁅⁆⁇⁈⁉⁊⁋⁌⁍⁎⁏⁐⁑⁒⁓
// U+2055–U+205E ⁕⁖⁗⁘⁙⁚⁛⁜⁝⁞
//  U+2190–U+23FF ∣

postfix func *% (block : ParserBlock) -> ParserBlock {
    return ParseZeroOrMore(block)
}


postfix func ?? (block : ParserBlock) -> ParserBlock {
    return ParseOptional(block)
}


postfix func + (block : ParserBlock) -> ParserBlock {
    return ParseOneOrMore(block)
}

func | (left : ParserBlock, right : ParserBlock) -> ParserBlock {
    return ParseChoice([left,right])
}

func choice(array : ParserBlock...) -> ParserBlock {
    return ParseChoice(array);
}

func + (left : ParserBlock, right : ParserBlock) -> ParserBlock {
    return ParseSequence([left,right])
}

func =>> (left : ParserBlock, right : ParseActionBlock) -> ParserBlock {
    return ParseAction(left, right)
}

func =>> (left : ParserBlock, right : () -> ()) -> ParserBlock {
    return ParseAction(left, right)
}

prefix func % ( array : [ParserBlock] ) -> ParserBlock {
    return ParseSequence(array)
}
func seq(array : [ParserBlock]) -> ParserBlock {
    return ParseSequence(array)
}
func seq(array : ParserBlock...) -> ParserBlock {
    return ParseSequence(array)
}

prefix func % ( char : Character ) -> ParserBlock {
    return ParseChar(char)
}

prefix func &% ( ref : ParseReference ) -> ParserBlock {
    return ParseWithReference( ref )
}

prefix func !% ( block : ParserBlock ) -> ParserBlock {
    return ParseNotPredicate(block)
}

prefix func !% ( char : Character ) -> ParserBlock {
    return ParseNotPredicate( ParseChar(char) )
}

prefix func !% ( array : [ParserBlock] ) -> ParserBlock {
    return ParseNotPredicate( ParseSequence(array) )
}

//MARK: Math Grammar

class NumberASTNode : MathASTNode {
    var number : Int = 0
    
    init(num : Int) {
        self.number = num
    }
    
    func value() -> Int {
        return number;
    }
}

class OpASTNode : MathASTNode {
    enum Op {
        case Multiply
        case Divide
        case Add
        case Subtract
        case Exponent
    }
    
    let left : MathASTNode;
    let right : MathASTNode;
    let op : Op;
    
    init(op : Op, left : MathASTNode, right : MathASTNode) {
        self.left = left
        self.right = right
        self.op = op
    }
    
    func value() -> Int {
        switch self.op {
        case Op.Add:
            return self.left.value() + self.right.value()
        case Op.Subtract:
            return self.left.value() - self.right.value()
        case Op.Multiply:
            return self.left.value() * self.right.value()
        case Op.Divide:
            return self.left.value() / self.right.value()
        case Op.Exponent:
            return Int(pow(Double(self.left.value()), Double(self.right.value())))
        }
    }
}


func MathGrammar(p: Parser) -> Bool
{
    var stack : [MathASTNode] = []
    var operators : [OpASTNode.Op] = []

    var createBinaryOpNode = { (p : Parser, range : Range<String.Index>) -> Bool in
        let right = stack.removeLast()
        let left  = stack.removeLast()
        let node = OpASTNode(op: operators.removeLast(), left: left, right: right)
        stack.append( node )
        return true
    }
    
    var parseNumber = ParseCharacterInRange("0", "9")+ =>>
    { (p : Parser, range : Range<String.Index>) -> Bool in
        let str = p.substr(range)
        if let num = str.toInt() {
            stack.append( NumberASTNode(num: num) )
        } else {
            assertionFailure("should never parse a non integer")
        }
        return true
    }
    

    var exprRef = ParseReference()
    
    let oParen = %"("
    let cParen = %")"
    let mul =    %"*" =>> { operators.append(OpASTNode.Op.Multiply) }
    let div =    %"/" =>> { operators.append(OpASTNode.Op.Divide) }
    let plus =   %"+" =>> { operators.append(OpASTNode.Op.Add) }
    let sub =    %"-" =>> { operators.append(OpASTNode.Op.Subtract) }
    let exp =    %"^" =>> { operators.append(OpASTNode.Op.Exponent) }
    
    var paren =         %[oParen, &%exprRef, cParen] =>> "ParenExpr"
    let value =         (parseNumber | paren) =>> "ValueExpr"
    let productCont =   %[ (mul | div | exp), value]  =>> createBinaryOpNode
    let product =       %[value, productCont*% ]
    let sumCont =       %[ plus | sub, product ] =>> createBinaryOpNode
    let expr =          %[ product, sumCont*% ]
    
    exprRef.block = expr
    
    let result = expr(p)
    if !result {
        return false
    }
    
    if let node = stack.last {
        println("math expression: \(node.value())")
    }
    
    return true
}

//var p = ParserImpl(buffer:"Hello Parser")
//var stringParser : ParserBlock = ParseString("Hello")
//
//if stringParser(p) {
//    println("Parse worked!")
//}
//var p2 = ParserImpl(buffer: "(1+2)^2+(3*2)-(5^2)")
//if MathGrammar(p2) {
//    println("math parser worked!")
//}

var p3 = GrammarParserImpl(buffer: "test = 'test'")
var grammar = ParserGrammar( )

if !grammar(p3) {
    println("parsing failed")
} else {
}

