//
//  main.swift
//  ParserBeta
//
//  Created by MattBaranowski on 9/21/14.
//  Copyright (c) 2014 MattBaranowski. All rights reserved.
//

import Foundation

struct Parser< Token, Result > {
    let p: Slice<Token> -> SequenceOf< (Result, Slice<Token>) >
}

extension Slice
{
    var head: T? {
        return self.isEmpty ? nil : self[0]
    }
    
    var tail: Slice<T> {
        return self.isEmpty ? self : self[(self.startIndex+1)..<self.endIndex]
    }
    
    var decompose: (head: T, tail: Slice<T>)? {
        return self.isEmpty ? nil : (self[self.startIndex], self.tail)
    }
}

func one<Type>(x: Type) -> SequenceOf<Type> {
        return SequenceOf(GeneratorOfOne(x))
}

func none<A>() -> SequenceOf<A> {
    return SequenceOf(GeneratorOf { return nil })
}

struct JoinedGenerator<A> : GeneratorType {
    typealias Element = A
    
    var generator: GeneratorOf<GeneratorOf<A>>
    var current: GeneratorOf<A>?
    
    init(_ g: GeneratorOf<GeneratorOf<A>>) {
        generator = g
        current = generator.next()
    }
    
    mutating func next() -> A?
    {
        if var c = current {
            if let x = c.next() {
                return x
            } else {
                current = generator.next()
                return next()
            }
        }
        return nil
    }
}

func map<A,B>(var g: GeneratorOf<A>, f: A -> B) -> GeneratorOf<B> {
    return GeneratorOf { map(g.next(), f) }
}

func map<A,B>(s:SequenceOf<A>, f : A -> B) -> SequenceOf<B> {
    return SequenceOf {map(s.generate(),f)}
}

func join<A>(s: SequenceOf<SequenceOf<A>>) -> SequenceOf<A> {
    return SequenceOf { JoinedGenerator(map(s.generate()) { $0.generate() }) }
}

func +<A>(l: SequenceOf<A>, r: SequenceOf<A>) -> SequenceOf<A> {
    return join(SequenceOf([l,r]))
}

func satisfy<Token>(condition: Token -> Bool) -> Parser<Token, Token>
{
    return Parser { x in
        if let (head, tail) = x.decompose {
            if condition(head) {
                return one((head, tail))
            }
        }
    
        return none()
    }
}

func token<Token: Equatable>(t: Token) -> Parser<Token, Token> {
    return satisfy { $0 == t }
}

infix operator <|> { associativity right precedence 130 }
func <|><Token, A>(left : Parser<Token,A>, right : Parser<Token, A>) -> Parser<Token, A> {
    return Parser { input in
        left.p(input) + right.p(input)
    }
}
